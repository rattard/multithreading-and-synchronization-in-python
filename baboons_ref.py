from __future__ import print_function
from threading import *
from collections import deque
from random import random, randint
from time import sleep
from timeit import Timer
import random
import sys
import logging

class Lightswitch:
    def __init__(self):
        self.mutex = Lock()
        self.count = 0

    def lock(self, sem):
        with self.mutex:
            self.count += 1
            if self.count == 1:
                sem.acquire()

    def unlock(self, sem):
        with self.mutex:
            self.count -= 1
            if self.count == 0:
                sem.release()


def act_as_baboon(my_id, init_side):
    global crossing_side
    side = init_side
    while num_crossings[my_id]<MAX_CROSSINGS:
        with turnstile:
            switches[side].lock(rope)
        with multiplex:
            with mutex:
                crossing_side = side
                crossing.add(my_id)
                waiting[side].remove(my_id)
            rng = random.Random()
            rng.seed(my_id)
            sleep(rng.random()/SPEED_FACTOR)  # crossing
            with mutex:
                crossing.remove(my_id)
                num_crossings[my_id]+=1
                waiting[1 - side].add(my_id)
        switches[side].unlock(rope)
        side = 1 - side

def report():
    while VERBOSE and bthreads.__len__()>0:
        sleep(1/SPEED_FACTOR)
        with mutex:
#            if not crossing_side:
#               continue
            print(repr(waiting[0]).rjust(30), end=' ')
            if crossing_side == 0:
                print('---', end='')
            else:
                print('<--', end='')
            print(repr(crossing).center(17), end=' ')
            if crossing_side == 0:
                print('-->', end=' ')
            else:
                print('---', end=' ')
            print(waiting[1])
        
def sim_baboons():
    del num_crossings[:]
    del bthreads[:]
    for i in range(NUM_BABOONS):
        bid, bside = i, randint(0, 1)
        waiting[bside].add(bid)
        bthreads.append(Thread(target=act_as_baboon, args=[bid, bside]))
        num_crossings.append(0)
    #Start the baboons
    reporter=Thread(target=report)
    reporter.start()
    for t in bthreads:
        t.start()
    for j in bthreads:
        j.join()
    for k in bthreads:
        bthreads.remove(k)
    print('Sim complete.')
    return
        

#Speed factor: Higher factor means faster times. RNG/Factor=Rope Time
#Max out around 50x-100x
#Average Time over 100 trials:0.727917023182 with SF=50, NT=100, RM=3, NB=10, MC=10
SPEED_FACTOR = 50
NUM_TRIALS = 1000

ROPE_MAX    = 3
NUM_BABOONS = 10
MAX_CROSSINGS = 10
VERBOSE =False
side_names  = ['west', 'east']
num_crossings =[]

if __name__ == '__main__':
    rope       = Lock()
    turnstile  = Lock()
    switches   = [Lightswitch(), Lightswitch()]
    multiplex  = Semaphore(ROPE_MAX)

    # reporting structures
    mutex         = Lock()
    waiting       = [set(), set()]
    crossing      = set()
    crossing_side = None

    bthreads   = []
    
    timer=Timer(sim_baboons)
    sims=timer.repeat(NUM_TRIALS, 1)
    print('Average Time over {1} trials:{0}\n {2}'.format(float(sum(sims)/NUM_TRIALS), NUM_TRIALS,sims))
