from __future__ import print_function
from threading import *
from collections import deque
from random import random, randint
from time import sleep
from timeit import Timer
import random
import sys
import logging

class Lightswitch:
    def __init__(self):
        self.mutex = Lock()
        self.count = 0

    def lock(self, sem):
        with self.mutex:
            self.count += 1
            if self.count == 1:
                sem.acquire()

    def unlock(self, sem):
        with self.mutex:
            self.count -= 1
            if self.count == 0:
                sem.release()


def act_as_baboon(my_id, init_side):
    global crossing_side
    side = init_side
    while num_crossings[my_id]<MAX_CROSSINGS:
        with turnstile:
            switches[side].lock(rope)
        with multiplex:
            with mutex:
                crossing_side = side
                crossing.add(my_id)
                if crossing.__len__<ROPE_MAX:
                    drain.release()
                waiting[side].remove(my_id)
            rng = random.Random()
            rng.seed(my_id)
            sleep(rng.random()/SPEED_FACTOR)  # crossing
            with mutex:
                crossing.remove(my_id)
                num_crossings[my_id]+=1
                waiting[1 - side].add(my_id)
        switches[side].unlock(rope)
        side = 1 - side
        
def drainer():
    while True:
    #block until drain is needed
        drain.acquire()
        global crossing_side
        if not waiting[crossing_side].__len__<((1-ROPE_YIELD_THRESHOLD)*(waiting[0].__len__+waiting[1].__len__)):
            continue
        else:
    #Don't allow any baboons to move into waiting area
            while crossing.__len__()>0:
                    mutex.acquire()
                    if crossing.__len__()>0:
                        mutex.release()
            mutex.release()
            turnstile.release()

def report():
    while VERBOSE and bthreads.__len__()>0:
        sleep(1/SPEED_FACTOR)
        with mutex:
            #if not crossing_side:
            #    continue
            print(repr(waiting[0]).rjust(30), end=' ')
            if crossing_side == 0:
                print('---', end='')
            else:
                print('<--', end='')
            print(repr(crossing).center(17), end=' ')
            if crossing_side == 0:
                print('-->', end=' ')
            else:
                print('---', end=' ')
            print(waiting[1])
        
def sim_baboons():
    del num_crossings[:]
    del bthreads[:]
    for i in range(NUM_BABOONS):
        bid, bside = i, randint(0, 1)
        waiting[bside].add(bid)
        bthreads.append(Thread(target=act_as_baboon, args=[bid, bside]))
        num_crossings.append(0)
    #Start the baboons
    reporter=Thread(target=report)
    reporter.start()
    for t in bthreads:
        t.start()
    for j in bthreads:
        j.join()
    for k in bthreads:
        bthreads.remove(k)
    global SIM_ID,NUM_TRIALS
    #print('Sim {0}/{1} complete.'.format(SIM_ID,NUM_TRIALS))
    SIM_ID+=1
    return
        

#Speed factor: Higher factor means faster times. RNG/Factor=Rope Time, Print Times factored in (1/Factor)
#Max out around 50x-100x for pure numerical analysis
#Ref Avg Time:0.727917023182
#with SF=50, NT=100, RM=3, NB=10, MC=10, 
#THRESHOLD=Varying:


SIM_ID=1
SPEED_FACTOR = 50
NUM_TRIALS = 100
REF_SPEED= 0.727917023182
ROPE_MAX    = 3
NUM_BABOONS = 10
MAX_CROSSINGS = 10
VERBOSE =False
ROPE_YIELD_THRESHOLD= .3
side_names  = ['west', 'east']
num_crossings =[]

if __name__ == '__main__':
    rope       = Lock()
    turnstile  = Lock()
    switches   = [Lightswitch(), Lightswitch()]
    multiplex  = Semaphore(ROPE_MAX)
    drain = Semaphore(0)

    # reporting structures
    mutex         = Lock()
    waiting       = [set(), set()]
    crossing      = set()
    crossing_side = None

    bthreads   = []
    for i in range(1,10):
        ROPE_YIELD_THRESHOLD=float(.1*i)
        timer=Timer(sim_baboons)
        sims=timer.repeat(NUM_TRIALS, 1)
        print('Average Time over {1} trials for {3:2f} :{0} seconds\n {2}'.format(float(sum(sims)/NUM_TRIALS), NUM_TRIALS,sims,ROPE_YIELD_THRESHOLD))
        print('{0:f}%'.format(100*(float(sum(sims)/NUM_TRIALS)-REF_SPEED)/REF_SPEED))