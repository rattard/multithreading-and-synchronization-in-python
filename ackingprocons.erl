-module(prod_cons_v2).
-export([start/0]).
-define(MAX_VAL, 10).
-define(MAX_AHEAD, 3).

producer(Val, Consumer, Ahead) ->
    if Val =:= ?MAX_VAL -> 
            Consumer ! terminate;
       Ahead =:= ?MAX_AHEAD ->
            io:format("P: throttling!~n"),
            receive
                ack -> producer(Val, Consumer, Ahead - 1)
            end;
       true ->
            timer:sleep(random:uniform(500)),  % producing!
            Consumer ! {self(), Val},
            receive
                ack -> io:format("P: got confirmation, ahead by ~w~n", [Ahead]),
		       acknowledge(Val + 1, Consumer, Ahead-1)
            end
    end.
            
consumer() ->
    receive
        terminate -> done;
        {Producer, Val} -> io:format("C: got ~w~n", [Val]),
                           timer:sleep(random:uniform(1000)),  % consuming
                           Producer ! ack,
                           consumer()
    end.

acknowledge(Val, Consumer, Ahead) ->
	receive
		ack -> io:format("P: got confirmation, ahead by ~w~n", [Ahead]),
			acknowledge(Val, Consumer, Ahead-1)
	after 
		0 ->producer(Val+1,Consumer, Ahead+1)
	end
end.

start() ->
    C = spawn(fun consumer/0),
    spawn(fun() -> producer(0, C, 0) end).
