'''
Created on Jun 19, 2012

@author: Ryan Attard
'''
from threading import Semaphore, Lock, Thread
from collections import deque
from random import random, randint
from time import sleep
import sys
import logging
NUM_BALLS_VENDOR = 1000
NUM_BALLS_BUCKET = 10
NUM_BALLS_INITIAL_GOLFER = 0
NUM_BALLS_INITIAL_FIELD = 0
NUM_GOLFERS = 10
NUM_VENDORS = 1
TOTAL_BALLS=NUM_BALLS_VENDOR+(NUM_GOLFERS*NUM_BALLS_INITIAL_GOLFER)+NUM_BALLS_INITIAL_FIELD
golfer_threads = []
golfer_balls=[]
vendor_balls = 0
vendor_queue = []
field_balls = 0

#Straightforward, Behavior for Golfer Threads
#Hit balls while there's some left
#Else get balls!
def golf(gid):
    while True:
        if golfer_balls[gid]==0:
            get_balls(gid)
        while golfer_balls[gid]>0:
            hit_ball(gid)
        
def get_balls(gid):
#mutex on vendor balls if vendor isn't empty
        vendor_queue.append(gid)
        ballvendor.acquire()        
        global vendor_balls
        while vendor_balls >= 0:
            if  vendor_balls > NUM_BALLS_BUCKET:
                #print('Golfer {:2d} is getting a bucket of {:2d} balls \n'.format(gid, NUM_BALLS_BUCKET))
                golfer_balls[gid]+=NUM_BALLS_BUCKET
                vendor_balls-=NUM_BALLS_BUCKET
                vendor_queue.remove(gid)
                ballvendor.release()
                return
#Signal the ball reutrn
            else:
                    ballreturn.release()
                    ballreturn.acquire()  

            
def hit_ball(gid):
    balllock.acquire()
    global field_balls
    golfer_balls[gid]-=1
    field_balls+=1
    balllock.release()
    sleep(.001)

            
def return_balls():
        while True:
            ballreturn.acquire()
            balllock.acquire()
            print('Collecting Balls... Golfers in line:{0} '.format(vendor_queue))
            global field_balls, vendor_balls
            vendor_balls+=field_balls
            print('Returned {0} balls to vending machine '.format(field_balls))
            field_balls=0
            print('Stash balls:{0}'.format(sum(golfer_balls)))
            print('% Of Balls Remaining:{0:2d}%, {1} stash+{2} vendor/{3} total'.format((100*(sum(golfer_balls)+vendor_balls)/TOTAL_BALLS),sum(golfer_balls),vendor_balls,TOTAL_BALLS))
            balllock.release()
            ballreturn.release()
            sleep(.1)
        
        
if __name__ == '__main__':
    balllock = Lock()
    ballvendor = Lock()
    ballreturn = Semaphore(0)
    vendor_balls = NUM_BALLS_VENDOR
    field_balls = NUM_BALLS_INITIAL_FIELD
    for i in range(NUM_GOLFERS):
        gid= i
        golfer_balls.append(NUM_BALLS_INITIAL_GOLFER)
        golfer_threads.append(Thread(target=golf, args=[gid]))
    for j in golfer_threads:
        j.start()
    return_boy = Thread(target=return_balls,args=[])
    return_boy.start()
    pass

